package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import models.BKPMDocument;
import models.SecureLoginRequest;
import models.UploadDocumentRequest;
import play.libs.Json;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Result;

import java.util.Base64;
import java.util.List;

/**
 * This controller contains all action to handle HTTP requests for this application
 */
public class AppController extends Controller {

  @BodyParser.Of(BodyParser.Json.class)
  public Result secure_login() {

    try {
      JsonNode jsonNode = request().body().asJson();
      SecureLoginRequest secureLoginRequest = Json.fromJson(jsonNode, SecureLoginRequest.class);

      /*
       * TODO Integrate with encryption/decryption services
       */

      return ok(Json.toJson(secureLoginRequest));
    } catch (IllegalStateException e) {
      return badRequest();
    }
  }

  @BodyParser.Of(BodyParser.Json.class)
  public Result upload_document() {
    try {
      JsonNode jsonNode = request().body().asJson();
      UploadDocumentRequest uploadDocumentRequest = Json.fromJson(jsonNode, UploadDocumentRequest.class);

      byte[] documentData = Base64.getDecoder().decode(uploadDocumentRequest.getEncodedFile());

      BKPMDocument bkpmDocument = new BKPMDocument(uploadDocumentRequest.getFileName(), documentData);
      bkpmDocument.save();

      return ok(Json.toJson(bkpmDocument.getId()));
    } catch (Exception e) {
      return badRequest(e.getMessage());
    }
  }

  public Result retrieve_documents() {
    try {
      List<BKPMDocument> bkpmDocuments = BKPMDocument.find.all();

      System.out.println(bkpmDocuments.size());

      return ok(Json.toJson(bkpmDocuments));
    } catch (Exception e) {
      return badRequest(e.getMessage());
    }
  }

  public Result sign_document() {
    return ok();
  }

  public Result validate_document() {
    return ok();
  }
}
