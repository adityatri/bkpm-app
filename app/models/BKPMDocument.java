package models;

import com.avaje.ebean.Model;
import play.data.validation.Constraints;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;

@Entity
public class BKPMDocument extends Model {

  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Id
  private Long id;

  @Constraints.Required
  private String name;

  @Lob
  @Constraints.Required
  private byte[] rawContent;

  @Lob
  private byte[] signedContent;

  private long lastUpdatedTimestamp;

  private long signedTimestamp;

  private boolean isActive;

  public BKPMDocument() {}

  public BKPMDocument(String name, byte[] rawContent) {
    this.name = name;
    this.rawContent = rawContent;
    this.lastUpdatedTimestamp = System.currentTimeMillis();
  }

  public static Finder<String, BKPMDocument> find = new Finder<String, BKPMDocument>(BKPMDocument.class);

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public byte[] getRawContent() {
    return rawContent;
  }

  public void setRawContent(byte[] rawContent) {
    this.rawContent = rawContent;
  }

  public byte[] getSignedContent() {
    return signedContent;
  }

  public void setSignedContent(byte[] signedContent) {
    this.signedContent = signedContent;
  }

  public long getSignedTimestamp() {
    return signedTimestamp;
  }

  public void setSignedTimestamp(long signedTimestamp) {
    this.signedTimestamp = signedTimestamp;
  }

  public long getLastUpdatedTimestamp() {
    return lastUpdatedTimestamp;
  }

  public void setLastUpdatedTimestamp(long lastUpdatedTimestamp) {
    this.lastUpdatedTimestamp = lastUpdatedTimestamp;
  }

  public boolean isActive() {
    return isActive;
  }

  public void setActive(boolean active) {
    isActive = active;
  }
}
