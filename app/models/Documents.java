package models;

import com.avaje.ebean.Model;
import play.data.validation.Constraints;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Documents extends Model {

  @Id
  private String name;

  @Constraints.Required
  private String content;

  private String rawContent;
  private String signedContent;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getContent() {
    return content;
  }

  public void setContent(String content) {
    this.content = content;
  }

  public static Finder<String, Documents> find = new Finder<String, Documents>(Documents.class);
}
