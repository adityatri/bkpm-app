package models;

public class UploadDocumentRequest {
  private String fileName;
  private String encodedFile;

  public UploadDocumentRequest() {}

  public String getFileName() {
    return fileName;
  }

  public void setFileName(String fileName) {
    this.fileName = fileName;
  }

  public String getEncodedFile() {
    return encodedFile;
  }

  public void setEncodedFile(String encodedFile) {
    this.encodedFile = encodedFile;
  }
}
