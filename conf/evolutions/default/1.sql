# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table bkpmdocument (
  id                            bigint auto_increment not null,
  name                          varchar(255),
  raw_content                   longblob,
  signed_content                longblob,
  last_updated_timestamp        bigint,
  signed_timestamp              bigint,
  is_active                     tinyint(1) default 0,
  constraint pk_bkpmdocument primary key (id)
);

create table documents (
  name                          varchar(255) not null,
  content                       varchar(255),
  raw_content                   varchar(255),
  signed_content                varchar(255),
  constraint pk_documents primary key (name)
);


# --- !Downs

drop table if exists bkpmdocument;

drop table if exists documents;

